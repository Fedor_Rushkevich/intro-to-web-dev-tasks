/* eslint-disable import/prefer-default-export */
const footerListItems = [
  { title: "About Us" },
  { title: "Contact" },
  { title: "CR Info" },
  { title: "Twitter" },
  { title: "Facebook" },
];

export function getFooterListItems() {
  return [...footerListItems];
}
