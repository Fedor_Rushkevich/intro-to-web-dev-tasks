/* eslint-disable import/prefer-default-export */
const headerItems = [
  {
    title: "Simo",
    href: "/",
    id: "1",
  },
  {
    title: "Discover",
    href: "/discover",
    id: "2",
  },
  {
    title: "Join",
    href: "/sign-in",
    id: "3",
  },
  {
    title: "Sign In",
    href: "/sign-in",
    id: "4",
  },
];

export function getHeaderItems() {
  return [...headerItems];
}
