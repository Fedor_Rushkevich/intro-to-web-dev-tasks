/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
import { getHeaderItems } from "../../api/header-items";
import "./header.css";
import logo from "../../../assets/icons/logo.svg";

function createHeader() {
  const container = document.createElement("header");
  const navBar = document.createElement("nav");

  navBar.id = "nav-bar";
  navBar.className = "header__nav-bar";

  // const img = document.createElement("img");

  // img.src = logo;
  // img.alt = "logo";
  // img.className = "header__nav-bar__logo";
  // navBar.append(img);

  const list = document.createElement("ul");

  list.className = "header__nav-bar__list";
  navBar.append(list);

  const arr = getHeaderItems();

  arr.forEach((element) => {
    const li = document.createElement("li");

    const link = document.createElement("a");

    // link.href = element.href;
    li.append(link);

    const button = document.createElement("button");
    button.className = "header__nav-bar__buttons";
    button.id = element.id;
    button.innerHTML = element.title;
    button.href = element.href;
    link.append(button);
    list.append(li);
  });

  container.append(navBar);
  return container;
}

export function setupHeader(parent) {
  const header = createHeader();
  parent.append(header);
}
