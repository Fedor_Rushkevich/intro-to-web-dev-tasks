/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
import "./body.css";
import main from "../../../assets/images/background-page-landing.png";

function createBody() {
  const container = document.createElement("div");

  const content = document.createElement("div");

  content.className = "body__content";
  content.id = "body";
  container.className = "body__container";
  const h1 = document.createElement("div");

  container.id = "body-container";
  h1.innerHTML = "Feel the music";
  h1.className = "body__container__text-big";
  content.append(h1);

  const text = document.createElement("div");

  text.innerHTML = "Stream over 10 million songs with one click";
  text.className = "body__container__text-small";
  content.append(text);

  const link = document.createElement("a");
  const button = document.createElement("button");

  button.innerHTML = "Join Now";
  link.href = "/sign-in";
  button.className = "body__container__button";
  link.append(button);
  content.append(link);

  const img = document.createElement("img");

  img.src = main;
  img.alt = "main-page background image";
  img.className = "body__container__image";
  img.id = "body-img";
  container.append(content);
  container.append(img);

  return container;
}

export function setupBody(parent) {
  const body = createBody();

  parent.append(body);
}
