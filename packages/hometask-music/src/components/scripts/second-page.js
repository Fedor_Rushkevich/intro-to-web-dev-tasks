/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */

import music from "../../../assets/images/music-titles.png";
import { getBodyItems } from "../../api/body-items";
import { setupHeader } from "../header/header";
import { createBody3 } from "./third-page";

export function setupBody2() {
  const join = document.getElementById("3");
  setupHeader();
  join.remove();

  const bodyImage = document.getElementById("body-img");

  bodyImage.remove();

  const content = document.getElementById("body-container");

  content.removeAttribute("style");
  content.style.padding = "0";

  const body = document.getElementById("body");

  body.removeAttribute("style");
  const container = createBody2();
  return container;
  // const header = document.getElementById("body");

  // header.replaceChildren(container);
}

export function createBody2() {
  setupHeader(document.getElementById("app"));
  const container = document.createElement("div");

  container.className = "body2";

  const leftP = document.createElement("div");

  leftP.className = "body2__main-content";

  const bigText = document.createElement("div");

  bigText.className = "body2__big-text";
  bigText.innerHTML = "Discover new music";
  const arr = getBodyItems();

  const buttons = document.createElement("div");

  buttons.className = "body2__buttons";

  arr.forEach((element) => {
    const button = document.createElement("button");

    button.innerHTML = element.title;
    button.className = "body2__button";
    buttons.append(button);
  });

  const smallText = document.createElement("div");
  smallText.className = "body2__small-text";
  smallText.innerHTML =
    "By joing you can benefit by listening to the last albums released";

  leftP.append(bigText);
  leftP.append(buttons);
  leftP.append(smallText);

  const img = document.createElement("img");

  img.className = "body2__img";
  img.src = music;

  container.append(leftP);
  container.append(img);

  return container;
}
