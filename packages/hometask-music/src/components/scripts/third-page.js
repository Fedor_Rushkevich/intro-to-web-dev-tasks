/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */

import joinI from "../../../assets/images/background-page-sign-up.png";

export function setupBody3() {
  const join = document.getElementById("3");

  join.remove();

  const bodyImage = document.getElementById("body-img");

  bodyImage.remove();

  const content = document.getElementById("body-container");

  content.removeAttribute("style");
  content.style.padding = "0";

  const body = document.getElementById("body");

  body.removeAttribute("style");

  const container = createBody3();

  const header = document.getElementById("body");

  header.replaceChildren(container);
}

export function createBody3() {
  const container = document.createElement("div");

  container.className = "body3";

  const registrationForm = document.createElement("div");
  registrationForm.classList.add("body3__registration-form");

  const registrationTable = document.createElement("table");
  registrationTable.classList.add("body3__registration-table");

  const nameRow = document.createElement("tr");

  const nameLabelCell = document.createElement("td");
  nameLabelCell.classList.add("body3__registration-label");
  nameLabelCell.textContent = "Name:";

  const nameInputCell = document.createElement("td");
  const nameInput = document.createElement("input");
  nameInput.type = "text";
  nameInput.classList.add("body3__registration-input");
  nameInput.placeholder = "Enter your name";

  nameInputCell.appendChild(nameInput);

  nameRow.appendChild(nameLabelCell);
  nameRow.appendChild(nameInputCell);

  registrationTable.appendChild(nameRow);

  const passwordRow = document.createElement("tr");

  const passwordLabelCell = document.createElement("td");
  passwordLabelCell.classList.add("body3__registration-label");
  passwordLabelCell.textContent = "Password:";

  const passwordInputCell = document.createElement("td");
  const passwordInput = document.createElement("input");
  passwordInput.type = "password";
  passwordInput.classList.add("body3__registration-input");
  passwordInput.placeholder = "Enter your password";

  passwordInputCell.appendChild(passwordInput);

  passwordRow.appendChild(passwordLabelCell);
  passwordRow.appendChild(passwordInputCell);

  registrationTable.appendChild(passwordRow);

  const emailRow = document.createElement("tr");

  const emailLabelCell = document.createElement("td");
  emailLabelCell.classList.add("body3__registration-label");
  emailLabelCell.textContent = "Email:";

  const emailInputCell = document.createElement("td");
  const emailInput = document.createElement("input");
  emailInput.type = "email";
  emailInput.classList.add("body3__registration-input");
  emailInput.placeholder = "Enter your email";

  emailInputCell.appendChild(emailInput);

  emailRow.appendChild(emailLabelCell);
  emailRow.appendChild(emailInputCell);

  registrationTable.appendChild(emailRow);

  registrationForm.appendChild(registrationTable);

  container.append(registrationForm);

  const img = document.createElement("img");

  img.className = "body3__img";
  img.src = joinI;
  container.append(img);

  return container;
}
