/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */

import { setupBody2 } from "../scripts/second-page";

export function discover() {
  const container = document.createElement("div");
  container.append(setupBody2());

  const parent = document.getElementById("content");
  parent.replaceChildren(container);
}
