/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
// import { discover } from "./discover";

import { discover } from "./discover";

const routes = {
  //   "/": simo,
  "/discover": discover,
  //   "/join": join,
  //   "/sign-in": signIn,
};

function renderRoute(e) {
  e.preventDefault();
  console.log("working");
  console.log(e.target);
  if (e.target.href) {
    console.log(e.target.href);
    routes[e.target.href]();
    history.pushState({}, "", e.target.pathname);
    e.target.classList.add("hidden");
  }
}

export const startFunction = () => {
  const mainMenu = document.querySelector("#nav-bar");
  mainMenu.addEventListener("click", renderRoute);
};
