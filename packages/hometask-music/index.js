/* eslint-disable no-undef */

import "./styles/style.css";
import { setupFooter } from "./src/components/footer/footer";
import { setupHeader } from "./src/components/header/header";
import { setupBody } from "./src/components/body/body";
import { setupBody2 } from "./src/components/scripts/second-page";
import { setupBody3 } from "./src/components/scripts/third-page";
import { startFunction } from "./src/components/routs/routs";

const mainContent = document.createElement("div");

mainContent.id = "content";

document.getElementById("app").append(mainContent);

setupHeader(document.getElementById("content"));

setupBody(document.getElementById("content"));

setupFooter(document.getElementById("app"));

// const discoverButton = document.getElementById("2");

// discoverButton.addEventListener("click", (event) => {
//   event.preventDefault();
//   setupBody2();
// });

// const SignInButton = document.getElementById("4");

// SignInButton.addEventListener("click", (e) => {
//   e.preventDefault();
//   setupBody3();
// });

// const homeButton = document.getElementById("1");

// homeButton.addEventListener("click", (ev) => {
//   ev.preventDefault();
//   firstPage();
// });

startFunction();
