/* eslint-disable no-undef */

const hamburger = document.getElementById("hamburger");
const navUl = document.getElementById("nav-ul");

hamburger.addEventListener("click", () => {
  navUl.classList.toggle("show");
});

const sliderm = new Sliderm("#exampe-slider", {
  arrow: true,
  pagination: false,
  grouping: false,
  loop: true,
  preview: false,
  columns: 3,
  duration: 500,
  spacing: 10,
  align: "center",
});

sliderm.on("slide.start", () => {});

sliderm.on("slide.end", () => {});
